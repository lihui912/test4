<?php

namespace App;

use Exception;
use PhpOffice\PhpWord\TemplateProcessor;


class ClaimFormGenerator {

    const COMPANY_DJZ = 'djz';
    const COMPANY_NEWERA = 'newera';

    const CLASS_TYPE_LECTURER = 'L';
    const CLASS_TYPE_TUTORIAL = 'T';

    private const TABLE_ROW = 27;

    private $data = [];

    private $templatePath = '';
    private $outputPath = '';

    public function __construct(string $templatePath = '', string $outputPath = '') {
        $this->templatePath = $templatePath;
        $this->outputPath = $outputPath;
    }

    public function setData(array $data = []) {
        $this->data = $data;
    }

    public static function getEmptyForm(): array {
        $form = [];
        $form['cname'] = '';
        $form['ename'] = '';
        $form['year'] = '';
        $form['monthCh'] = '';
        $form['monthEn'] = '';
        $form['departmentName'] = '';
        $form['staffId'] = '';
        $form['subjectCode'] = '';
        $form['dateSign'] = '';
        $form['company'] = '';

        $form['classRecord'] = '';

        return $form;
    }

    public static function getEmptyClassRecord(): array {
        return ['classDate' => '',
         'classTime' => '',
         'classDuration' => '',
         'classType' => '',
         'classSum' => '',
         'classRemark' => ''];
    }

    public function generateDocument() {
        if(true === empty($this->data)) {
            throw new Exception('Data not set.');
        }
        $data = $this->data;

        $word = new TemplateProcessor($this->templatePath);

        $word->setValue('ename', $data['ename']);
        $word->setValue('cname', $data['cname']);
        $word->setValue('year', $data['year']);
        $word->setValue('monthCh', $data['monthCh']);
        $word->setValue('monthEn', $data['monthEn']);
        $word->setValue('departmentName', $data['departmentName']);
        $word->setValue('staffId', $data['staffId']);
        $word->setValue('subjectCode', $data['subjectCode']);
        $word->setValue('dateSign', $data['dateSign']);

        if(ClaimFormGenerator::COMPANY_DJZ === $data['company']) {
            $word->setValue('companyDjz', '✔');
            $word->setValue('companyNewera', '');

        } else {
            $word->setValue('companyDjz', '');
            $word->setValue('companyNewera', '✔');

        }

        // 27 rows
        $word->cloneRow('classDate', ClaimFormGenerator::TABLE_ROW);

        $classDurationLTotal = 0;
        $classDurationTTotal = 0;
        $classSumLTotal = 0;
        $classSumTTotal = 0;
        $index = 1;
        foreach($data['classRecord'] as $row) {
            if(ClaimFormGenerator::CLASS_TYPE_LECTURER === $row['classType']) {
                $classDurationLTotal += $row['classDuration'];
                $classSumLTotal += $row['classSum'];
            } else if(ClaimFormGenerator::CLASS_TYPE_TUTORIAL === $row['classType']) {
                $classDurationTTotal += $row['classDuration'];
                $classSumTTotal += $row['classSum'];
            }

            foreach($row as $key => $value) {
                $word->setValue($key . '#' . $index, $value);
            }
            $index++;
        }

        $emptyData = $this->getEmptyClassRecord();

        if(ClaimFormGenerator::TABLE_ROW > $index) {
            while(ClaimFormGenerator::TABLE_ROW >= $index) {
                foreach($emptyData as $key => $value) {
                    $word->setValue($key . '#' . $index, $value);
                }
                $index++;
            }
        }

        $word->setValue('classDurationLTotal', $classDurationLTotal);
        $word->setValue('classDurationTTotal', $classDurationTTotal);

        $word->setValue('classSumLTotal', $classSumLTotal);
        $word->setValue('classSumTTotal', $classSumTTotal);
        $word->setValue('classSumTotal', $classSumLTotal + $classSumTTotal);


        $outputFile = $this->outputPath . '/merged' . date('Ymdhis') . '.docx';

        $word->saveAs($outputFile);

        return $outputFile;
    }

}
