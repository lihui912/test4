<?php

include __DIR__ . '/vendor/autoload.php';

use Twig\Loader\FilesystemLoader;
use Twig\Environment;
use GuzzleHttp\Psr7\ServerRequest;

use App\ClaimFormGenerator;

$environment = getenv('ENVIRONMENT');

$templatePath = __DIR__ . '/template';
$twigLoader = new FilesystemLoader($templatePath);

$twig = new Environment($twigLoader);

if('POST' === $_SERVER['REQUEST_METHOD']) {

    $template = __DIR__ . '/wordTemplates/claimform.docx';
    $outputFolder = __DIR__;

    $claimform = new ClaimFormGenerator($template, $outputFolder);

    $request = ServerRequest::fromGlobals();

    $input = $request->getParsedBody();

    $claimformData = ClaimFormGenerator::getEmptyForm();
    $claimformData['cname'] = $input['inputCname'] ?? '';
    $claimformData['ename'] = $input['inputEname'] ?? '';
    $claimformData['year'] = $input['inputYear'] ?? '';
    $claimformData['monthCh'] = $input['inputMonth'] ?? '';
    $claimformData['monthEn'] = $input['inputMonth'] ?? '';
    $claimformData['departmentName'] = $input['inputDepartmentName'] ?? '';
    $claimformData['staffId'] = $input['inputStaffId'] ?? '';
    $claimformData['subjectCode'] = $input['inputSubjectCode'] ?? '';
    $claimformData['dateSign'] = $input['dateSign'] ?? date('d-m-Y');;
    $claimformData['company'] = $input['inputCompany'] ?? '';

    $rowCount = count($input['inputClassDate'] ?? []);
    $classRecordList = [];
    for($i = 0; $i < $rowCount; $i++) {
        $classRecord = ClaimFormGenerator::getEmptyClassRecord();

        $classRecord['classDate'] = $input['inputClassDate'][$i];
        $classRecord['classTime'] = $input['inputClassTime'][$i];
        $classRecord['classDuration'] = $input['inputClassDuration'][$i];
        $classRecord['classType'] = $input['inputClassType'][$i];
        $classRecord['classSum'] = $input['inputClassRevenue'][$i];
        $classRecord['classRemark'] = '';

        $classRecordList[] = $classRecord;
    }
    $claimformData['classRecord'] = $classRecordList;

    $claimform->setData($claimformData);

    $claimFile = $claimform->generateDocument();

    $newFileHandle = tmpfile();
    $newFile = stream_get_meta_data($newFileHandle)['uri'];

    copy($claimFile, $newFile);

    unlink($claimFile);
    $fp = fopen($newFile, 'rb');

    ob_clean();
    header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
    header('Content-Length: ' . filesize($newFile));
    header('Content-Disposition: attachment; filename="claimform.docx"');
    header('Content-Transfer-Encoding: binary');
    fpassthru($fp);
    ob_end_flush();

} else {
    ob_start();
    $data = [];
    $data['default'] = [];
    $data['default']['year'] = date('Y');
    $data['default']['month'] = date('n');

    $data['environment'] = $environment;

    echo $twig->render('index.html', $data);

    ob_end_flush();
}

